import React from "react";
import "./helloComponent.css";
const HelloWorldComponent = () => {
	return (
		<div className="card">
			<p className="text-p">
				Hello World functional component (statles Component)
			</p>
		</div>
	);
};
export default HelloWorldComponent;
