import React from "react";
import "./YoutubeComp.css";

const YoutubeComp = (props) => {
	return (
		<div className="youtube-wrapper">
			<div className="img-thumb">
				<img src={props.img} alt="" />
				<p className="time">{props.time}</p>
			</div>
			<p className="title">{props.title}</p>
			<p className="desc">{props.desc}</p>
		</div>
	);
};
YoutubeComp.defaultProps = {
	title: "No Title",
	time: "00.00",
	desc: "0x ditonton. 0 hari yang lalu",
	img: "https://media.istockphoto.com/vectors/no-thumbnail-image-vector-graphic-vector-id1147544806?b=1&k=6&m=1147544806&s=612x612&w=0&h=XZE8_6h1RsdeUL_IaNsEtY790zJ9P7gfguMWEqU0AXk=",
};
export default YoutubeComp;
