import React, { Component } from "react";

class CardProduct extends Component {
	state = {
		order: 1,
	};
	handleCounterChange = (v) => {
		this.props.onCounterChange(v);
	};
	handlePlus = () => {
		this.setState({
			order: this.state.order + 1,
		});
		this.handleCounterChange(this.state.order);
	};
	handleMinus = () => {
		if (this.state.order > 0) {
			this.setState({
				order: this.state.order - 1,
			});
			this.handleCounterChange(this.state.order);
		}
	};
	render() {
		return (
			<div className="card">
				<div className="img-thumb-prod">
					<img
						src="https://etanee-images.s3-ap-southeast-1.amazonaws.com/81891166-0a07-4e7c-8643-5d9b697cf39b"
						alt="product-img"
					/>
				</div>
				<p className="product-title">Daging</p>
				<p className="product-price">Rp. 200,000</p>
				<div className="counter">
					<button className="minus" onClick={this.handleMinus}>
						-
					</button>
					<input type="text" value={this.state.order} />
					<button className="plus" onClick={this.handlePlus}>
						+
					</button>
				</div>
			</div>
		);
	}
}
export default CardProduct;
