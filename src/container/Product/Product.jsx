import React, { Component, Fragment, useState } from "react";
import CardProduct from "./CardProduct/CardPorduct";
import "./Product.css";

export default class Product extends Component {
	state = {
		order: 1,
	};

	handleCounterChange = (newValue) => {
		this.setState({
			order: newValue,
		});
	};

	render() {
		return (
			<Fragment>
				<div className="header">
					<div className="logo">
						<p>Warung</p>
					</div>
					<div className="troley">
						<img src={process.env.PUBLIC_URL + "/troley.png"} alt="" />
						<div className="count">{this.state.order}</div>
					</div>
				</div>
				<CardProduct onCounterChange={this.handleCounterChange} />
			</Fragment>
		);
	}
}
